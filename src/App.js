import React , { useState , useEffect } from 'react';
import styled from 'styled-components';
import Logo from './cryptomonedas.png'
import Formulario from './components/Formulario';
import Axios from 'axios';
import Cotizacion from './components/Cotizacion';
import Spinner from './components/Spinner';


const Contenedor = styled.div`
   max-width: 900px;
   margin: 0 auto;
   @media ( min-width: 992px ){
       display: grid;
       grid-template-columns: repeat(2,1fr);
       column-gap: 2rem;
   }
`
const Imagen = styled.img`
   max-width: 100%;
   margin-top: 5rem;
`
const Heading = styled.h1`
  font-family: 'Bebas Neue';
  color:#FFF;
  text-align: left;
  font-size: 50px;
  font-weight: 700;
  margin-top: 50px;
  margin-bottom : 80px;
  &::after{
    content: '';
    width: 100px;
    height: 6px;
    background: #66A2fe;
    display: block;
  }
`
function App() {
  
  const [ moneda , setMoneda ] = useState('')
  const [ criptomoneda , setCriptomoneda ] = useState('')
  const [ cotizacion , setCotizacion ] = useState({})
  const [ vistaGlobal , setVistaGlobal] = useState(false)
  const [ loading , setLoading ] = useState(false)

  useEffect( () => {
    
    if( moneda === '' )return;

    ( async () => {
      setLoading(true)
      setVistaGlobal(false)
      const url = `https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${criptomoneda}&tsyms=${moneda}` 
      const res = await Axios.get(url)
      const { DISPLAY } = res.data
      const respuesta = DISPLAY[criptomoneda][moneda]
      setTimeout( () => {
       setCotizacion(respuesta)
       setVistaGlobal(true)
       setLoading(false)
      } , 1500 )
    })()


  }, [ moneda , criptomoneda] )

  return (
    <Contenedor>
      <div>
        <Imagen src={Logo} alt="Logo" />
      </div>
      <div>
        <Heading>Cotizador De Criptomonedas</Heading>
         <Formulario setMoneda={setMoneda}  setCriptomoneda={setCriptomoneda} />
         {
           loading ? <Spinner/> : null
         }
         {
           vistaGlobal ? <Cotizacion resultado={cotizacion} /> : null
         }
      </div>
    </Contenedor>
  );
}

export default App;
