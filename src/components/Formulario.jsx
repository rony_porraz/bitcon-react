import React , { useEffect , useState } from 'react'
import styled from 'styled-components'
import useMoneda from '../hooks/useMoneda'
import useCriptomonedas from '../hooks/useCriptomonedas'
import Axios from 'axios'
import Error from './Error'

const BotonSubmit = styled.input`
   margin-top: 20px;
   font-weight: bold;
   background: #66a2fe;
   padding: 10px; 
   border: none;
   width: 100%;
   border-radius: 10px;
   color:#FFF;
   transition:350ms ease;
   outline: 0;
   &:hover{
       background: #326Ac0;
       cursor: pointer;
   }
`

const Formulario = ({ setMoneda , setCriptomoneda }) => {
    
    // STATE 

    const [ allCrypto , setAllCrypto ] = useState([])
    const [ error , setError ] = useState(false)
 
    const MONEDAS = [
        { code: 'USD', name: 'Dolar De Estados Unidos' },
        { code: 'EUR', name: 'Euro' },
        { code: 'GBP', name: 'Libra Esterlina' },
        { code: 'MXN', name: 'Pesos Mexicanos' }
    ]
    
    // HOOKS

    const [ stateMoneda , SeleccionarMoneda  ] = useMoneda('Elige Moneda' , '' , MONEDAS)
    const [ stateCripto , SeleccionarCripto  ] = useCriptomonedas('Elige Criptomoneda' , '' , allCrypto)
    
    // FUNCTIONS 

    const cotizarMoneda = e => {
        e.preventDefault()
        if( stateMoneda === '' || stateCripto === '' ){
            setError(true)
            return
        }
        setError(false)
        setMoneda(stateMoneda)
        setCriptomoneda(stateCripto)
        //console.log(stateMoneda , stateCripto);
    }

    useEffect( () => {
        const consultarApi = async () => {
            const url = `https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD`
            const response = await Axios.get(url)
            const data =  response.data.Data;
            setAllCrypto(data)
        }
        consultarApi()   
    }, [])


    return (
        <form onSubmit={ cotizarMoneda }>
          {
             error 
             ? <Error/>
             : null
          }
          <SeleccionarMoneda/>

          <SeleccionarCripto/>

          <BotonSubmit type="submit" value="Cotizar"/>
        </form>
    )
}

export default Formulario
