import React from 'react'
import styled from 'styled-components'

const MensajeError = styled.p`
  background-color: #B7322C;
  padding: 1rem;
  color:#FFF;
  font-size: 25px;
  text-transform: uppercase;
  text-align:center;
  font-family: 'Bebas Neue';
`

const Error = () => {
    return (
        <MensajeError>
            complete Todos los campos
        </MensajeError>
    )
}

export default Error
