import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
 width: 100%;
 padding: 1rem;
 background :#FFF;
 max-width: 900px;
 margin: 1rem auto;
 box-sizing:border-box;
 border-radius: 10px;
`
const Parrafo = styled.p`
  font-family: 'Arial';
  font-size: 19px;
`
const Cotizacion = ({ resultado }) => {
    
    return (
        <Container>
            <Parrafo> El precio es: <span>{ resultado.PRICE }</span> </Parrafo>
            <Parrafo> El precio Más Alto Del dia: <span>{ resultado.HIGHDAY }</span> </Parrafo>
            <Parrafo> El precio Más Bajo Del dia: <span>{ resultado.LOWDAY }</span> </Parrafo>
            <Parrafo> Variación últimas 24 horas: <span>{ resultado.CHANGEPCT24HOUR }</span> </Parrafo>
            <Parrafo> Ultima Actualización: <span>{ resultado.LASTUPDATE }</span> </Parrafo>
        </Container>
    )
}

export default Cotizacion
