import React, { useState } from 'react'
import styled from 'styled-components'

const Label = styled.label`
    color:#FFF;
    font-family: 'Arial';
    margin: 10px 0;
`

const SelectComponent = styled.select`
    width: 100%;
    padding: 1rem;
    -webkit-appearance: none;
    border-radius: 10px; 
    border: 0;
    margin-top: 1rem;
    outline:0;
`
const Margin = styled.div`
  margin: 1rem 0;
`

const useCriptomoneda = ( data  , stateInicial , OPTIONS ) => {
    
    const [ state , setState ] = useState(stateInicial)

    const SeleccionarCripto = ( ) => (

        <Margin>
        <Label>{data}</Label>
        <SelectComponent onChange={ e => setState(e.target.value) } value={state} >
                <option value="">-- Seleccione Una Opcion --</option>
            {
                OPTIONS.map
                (
                   ele => <option key={ele.CoinInfo.Id} value={ele.CoinInfo.Name}>{ ele.CoinInfo.FullName }</option>
                )
            }
        </SelectComponent>
       </Margin>    

   )
   return [ state , SeleccionarCripto , setState ]
}

export default useCriptomoneda