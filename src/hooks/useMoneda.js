import React, { useState } from 'react'
import styled from 'styled-components'

const Label = styled.label`
    color:#FFF;
    font-family: 'Arial';
    margin: 3px;
`

const SelectComponent = styled.select`
    width: 100%;
    padding: 1rem;
    -webkit-appearance: none;
    border-radius: 10px; 
    border: 0;
    margin-top: 1rem;
    outline:0;
`

const useMoneda = ( data  , stateInicial , OPTIONS ) => {
    
    const [ state , setState ] = useState(stateInicial)

    const Seleccionar = ( ) => (

        <>
        <Label>{data}</Label>
        <SelectComponent onChange={ e => setState(e.target.value) } value={state} >
                <option value="">-- Seleccione Una Opcion --</option>
            {
                OPTIONS.map(
                  option => <option key={option.code}value={option.code}>{ option.name }</option>
                )
            }
        </SelectComponent>
       </>    

   )
   return [ state , Seleccionar , setState ]
}

export default useMoneda